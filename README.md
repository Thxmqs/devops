gcloud auth list // affiche la liste des comptes utilisateurs
gcloud config list project // liste ID de projet
gcloud compute networks create taw-custom-network --subnet-mode custom // créer le réseau personnalisé taw-custom-network
gcloud compute networks subnets create subnet-europe-west1 \ // créer le sous-réseau avec préfixe IP (même commande poru chaque sous-réseau)
   --network taw-custom-network \
   --region europe-west1 \
   --range 10.0.0.0/16 
   gcloud compute networks subnets list \ // afficher la liste des réseaux
   --network taw-custom-network
   gcloud compute firewall-rules create nw101-allow-http \ // créer la nouvelle règle de pare-feu
--allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 \
--target-tags http
gcloud compute firewall-rules create "nw101-allow-icmp" --allow icmp --network "taw-custom-network" --target-tags rules // règle ICMP
gcloud compute firewall-rules create "nw101-allow-internal" --allow tcp:0-65535,udp:0-65535,icmp --network "taw-custom-network" --source-ranges "10.0.0.0/16","10.2.0.0/16","10.1.0.0/16" // règles communications internes
gcloud compute firewall-rules create "nw101-allow-ssh" --allow tcp:22 --network "taw-custom-network" --target-tags "ssh" // règles SSH
gcloud compute firewall-rules create "nw101-allow-rdp" --allow tcp:3389 --network "taw-custom-network" // règles RDP
gcloud compute instances create us-test-01 \
--subnet subnet-europe-west1 \
--zone europe-west1-d \
--machine-type e2-standard-2 \
--tags ssh,http,rules // céation instance VM
gcloud compute instances create us-test-02 \
--subnet subnet-us-west1 \
--zone us-west1-c \
--machine-type e2-standard-2 \
--tags ssh,http,rules // création autre instance
gcloud compute instances create us-test-03 \
--subnet subnet-europe-west4 \
--zone europe-west4-a \
--machine-type e2-standard-2 \
--tags ssh,http,rules // création instance VM
gcloud compute instances create us-test-03 \
--subnet subnet-europe-west4 \
--zone europe-west4-a \
--machine-type e2-standard-2 \
--tags ssh,http,rules // création autre instance VM
ping -c 3 <us-test-02-external-ip-address> // requête ping vers us-test-02
sudo apt-get update
sudo apt-get -y install traceroute mtr tcpdump iperf whois host dnsutils siege // installation outils de performance

touch instance.tf // créer le fichier de config
terraform init // installation binaire fournisseur
terraform plan // créer un plan d'éexecution
terraform show // afficher l'état
